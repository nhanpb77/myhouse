//: Playground - noun: a place where people can play

import UIKit

class QuickSort {
    func quickSort(_ array : inout [Int], _ left : Int, _ right : Int) {
        if left >= right {
            return
        }
        var i : Int = left
        var j : Int = right
        
        
        let pivot : Int = array[(left + right) / 2]
        while i <= j {
            while array[i] < pivot {
                i += 1
            }
            while array[j] > pivot {
                j -= 1
            }
            if i <= j {
                let temp = array[i]
                array[i] = array[j]
                array[j] = temp
                i += 1
                j -= 1
            }
        }
        
        quickSort(&array, left, j)
        quickSort(&array, i, right)
    }
}
var  array : [Int] = [1,4,2,3,9,5,7,6,8,0]

let quickSort : QuickSort = QuickSort()
quickSort.quickSort(&array, 0, array.count - 1 )
print(array)
