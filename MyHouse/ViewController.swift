//
//  ViewController.swift
//  MyHouse
//
//  Created by Nhan Phung on 11/20/16.
//  Copyright © 2016 Nhan Phung. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate  {
    
    @IBOutlet weak var mapView: MKMapView!
    let locationManager : CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        mapView.delegate = self
        
        
        showMyHouseLocation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showMyHouseLocation() {
        let myCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 10.8326105, longitude: 106.72306895)
        let region : MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(myCoordinate, 1000, 1000)
        mapView.setRegion(region, animated: true)
        addAnnotationOnLocation(myCoordinate)
        
        
    }
    
    // Func add annotation on user location
    func addAnnotationOnLocation(_ coordinate : CLLocationCoordinate2D) {
        let point : MKPointAnnotation = MKPointAnnotation()
        point.coordinate = coordinate
        point.title = "My House"
        point.subtitle = "58/20 đường 47, Hiệp Bình Chánh, Thủ Đức, Tp. Hồ Chí Minh, Việt Nam"
        mapView.addAnnotation(point)
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let view : MKPinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        //
        if annotation.title! == "My House" {
            view.pinTintColor = UIColor.green
        }else{
            view.pinTintColor = UIColor.red
        }
        // Show title
        view.canShowCallout = true
        return view
        
    }
 
    @IBAction func selectTypesAction(_ sender: Any) {
        
        let alert : UIAlertController = UIAlertController(title: "", message: "Choose Type", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let standardAction : UIAlertAction = UIAlertAction(title: "Standard", style: .default, handler: {(UIAlertAction) in
        self.mapView.mapType = MKMapType.standard
        })
        let satelliteAction : UIAlertAction = UIAlertAction(title: "Satellite", style: .default, handler: {(UIAlertAction) in
        self.mapView.mapType = MKMapType.satellite
        })
        let hybridAction : UIAlertAction = UIAlertAction(title: "Hybrid", style: .default, handler: {(UIAlertAction) in
            self.mapView.mapType = MKMapType.hybrid
        })
        
        alert.addAction(standardAction)
        alert.addAction(satelliteAction)
        alert.addAction(hybridAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func myHouseAction(_ sender: Any) {
        showMyHouseLocation()
    }
    
    
}

