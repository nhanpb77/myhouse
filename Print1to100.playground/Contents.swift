//: Playground - noun: a place where people can play

import UIKit

class PrintNumber {
    
    func print1to100(_ number : Int) -> Void {
        switch checkNumber(number) {
        case "fizz":
            print("Fizz")
            break
        case "buzz":
            print("Buzz")
            break
        case "fizz&buzz":
            print("FizzBuzz")
            break
        default:
            print(number)
            break
        }
    }
    
    
  private func checkNumber(_ number : Int) -> String {
        if number % 3 == 0 && number % 5 == 0 {
            return "fizz&buzz"
        }else if number % 3 == 0 {
            return "fizz"
        }else if number % 5 == 0 {
            return "buzz"
        }
        return ""
    }
}

class PrintNumber2 {
    
    func print1to100(_ number : Int) -> Void {
        switch number {
        case number where number % 3 == 0 && number % 5 == 0:
            print("FizzBuzz")
            break
        case number where number % 3 == 0:
            print("Fizz")
            break
        case number where number % 5 == 0:
            print("Buzz")
            break
        default:
            print(number)
            break
        }
    }

    
}

class PrintNumber3 {
    func print1to100(_ number : Int) -> Void {
        switch true {
        case number % 3 == 0 && number % 5 == 0:
            print("FizzBuzz")
            break
        case number % 3 == 0:
            print("Fizz")
            break
        case number % 5 == 0:
            print("Buzz")
            break
        default:
            print(number)
            break
        }
    }

}


// main

//let printNumber : PrintNumber = PrintNumber()
//let printNumber : PrintNumber2 = PrintNumber2()
let printNumber : PrintNumber3 = PrintNumber3()
for i : Int in 1 ... 100 {
    printNumber.print1to100(i)
}



